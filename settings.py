import logging

REDIS_ADDRESS = 'redis://redisdb'
REDIS_QUEUE   = 'queue'

DB_URL   = 'postgresql://postgres@postgresdb:5432'
DB_NAME  = 'lemmit_db'
DB_DEBUG = False

LOG_PATH          = '/tmp/lemmit.log'
LOG_CONSOLE_LEVEL = logging.DEBUG
LOG_FILE_LEVEL    = logging.INFO
