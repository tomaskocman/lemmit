import abc
import logging


class AsyncClient(abc.ABC):
    @property
    @abc.abstractmethod
    def headers(self):
        raise NotImplementedError

    @headers.setter
    @abc.abstractmethod
    def headers(self, new_headers):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def timeout(self):
        raise NotImplementedError

    @timeout.setter
    @abc.abstractmethod
    def timeout(self, new_timeout):
        raise NotImplementedError

    @property
    @abc.abstractmethod
    def proxy(self):
        raise NotImplementedError

    @proxy.setter
    @abc.abstractmethod
    def proxy(self, new_proxy):
        raise NotImplementedError

    @abc.abstractmethod
    async def get_request(self, url):
        raise NotImplementedError

    @abc.abstractmethod
    async def post_request(self, url, data):
        raise NotImplementedError


class AsyncJsClient(metaclass=abc.ABCMeta):

    def __init__(self, logger_name: str):
        self._logger = logging.getLogger(logger_name)
        self._cookies = None
        self._timeout = None

    @property
    def timeout(self):
        return self._timeout

    @timeout.setter
    def timeout(self, timeout):
        self._timeout = timeout

    @property
    def cookies(self):
        return self._cookies

    @cookies.setter
    @abc.abstractmethod
    def cookies(self, cookies):
        pass

    @abc.abstractmethod
    async def get_request(self, url):
        pass

    @abc.abstractmethod
    async def save_screenshot_to(self, filepath):
        pass
