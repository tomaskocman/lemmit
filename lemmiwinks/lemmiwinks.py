import abc
import asyncio
import logging

import settings


class Lemmiwinks(metaclass=abc.ABCMeta):
    """Implementation of event based loop.
    Contains abstract method 'task_executor' that holds starting point of the execution.
    """
    def __init__(self):
        self.loop = asyncio.get_event_loop()
        self.loop.set_debug(enabled=True)

        self.logger = Logger.get_logger(self.__class__.__name__)

    @abc.abstractmethod
    async def task_executor(self):
        raise NotImplemented

    def run(self):
        self.logger.debug('Running event loop until completion of task executor')
        self.loop.run_until_complete(self.task_executor())

    def stop(self):
        self.logger.debug('Closing event loop')
        self.loop.close()


class Logger:
    @staticmethod
    def get_logger(name: str) -> logging.Logger:
        """Configures logger to be used in the module where is imported after calling 'get_logger'.
        Adds two handlers to basic logger. One for file, second for console.
        For each handler sets format of logs and log level.
        
        Args:
            name: Name of new logger instance.
            
        Returns:
            logging.Logger object.
        """
        logger = logging.getLogger(name)

        formatter = logging.Formatter('%(asctime)s | %(name)-15s | %(levelname)-7s | %(message)s',
                                      datefmt='%Y-%m-%d %H:%M:%S')

        file_handler = logging.FileHandler(settings.LOG_PATH)
        file_handler.setFormatter(formatter)
        file_handler.setLevel(settings.LOG_FILE_LEVEL)

        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        console_handler.setLevel(settings.LOG_CONSOLE_LEVEL)

        logger.addHandler(file_handler)
        logger.addHandler(console_handler)

        logger.setLevel(logging.DEBUG)

        return logger
