# Lemmiwinks
<img src="https://vignette.wikia.nocookie.net/southpark/images/b/b8/Lemmiwinks_%282%29.png/revision/latest?cb=20161218172346" width=110 height=150><br/>
<br/>
#### Authors: Bc. Tomáš Kocman, Ing. Libor Polčák, Ph.D.<br/>
**Disclaimer**: Lemmiwnks is a part of whole platform. It is not a standalone application!<br/>

Lemmiwinks serves for web page archivation. It recursively downloads all resources found on the page.<br/>
**Input for archivation:** JSON objects from Redis database.<br/>
**Output of archivation:** MAFF archive with metainformations stored in PostgreSQL database.

### Steps to run Lemmiwinks unit tests
```
> Ensure that Redis and PostgreSQL are running!
> source venv/bin/activate
> python -m pytest
```

### Steps to run Lemmiwinks
```
> Ensure that Redis and PostgreSQL are running!
> source venv/bin/activate
> python pharty3.py
```

### Acknowledgments

This work was supported by the Ministry of the Interior of the Czech Republic grant
number VI20172020062.