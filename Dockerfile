FROM python:3.6-slim

COPY . /root/lemmit/

VOLUME /root/lemmit/archived

WORKDIR /root/lemmit/

RUN apt update && \
	apt install -y gcc vim libmagic-dev && \
	python -m venv venv && \
	. venv/bin/activate && \
	python -m pip install -r requirements.txt && \
	echo 'alias ll="ls -all"' >> ~/.bashrc

CMD . venv/bin/activate;python pharty3.py
