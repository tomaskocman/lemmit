import asyncio
import pytest

import settings
from pharty3 import RedisWrapper

loop = asyncio.get_event_loop()


class TestRedis:
    def test_connection(self):
        r = RedisWrapper(settings.REDIS_ADDRESS)
        assert r.redis is None

        loop.run_until_complete(r.create_connection())
        assert r.redis is not None

        loop.run_until_complete(r.close_connection())
        assert r.redis is None

    @pytest.fixture(scope='function')
    def mock_redis_input(self):
        return b'{"url": ["http://www.fit.vutbr.cz/"], "job": ["basicplatform"], "spider": ["basic"], \
    "server": ["tk"], "date": ["2019-02-02 15:53:29.135539"], "headers": ["{b\'Date\': \
    [b\'Sat, 02 Feb 2019 14:53:28 GMT\'], b\'Server\': [b\'Apache\'], b\'Content-Location\': \
    [b\'index.php.en\'], b\'Vary\': [b\'negotiate,accept-language\'], b\'Tcn\': [b\'choice\'], \
    b\'Pragma\': [b\'no-cache\'], b\'X-Frame-Options\': [b\'deny\'], b\'Content-Type\': \
    [b\'text/html; charset=iso-8859-2\'], b\'Content-Language\': [b\'en\']}"], "htmlcontent": \
    ["<!DOCTYPE HTML PUBLIC \\"-//W3C//DTD HTML 4.01//EN\\">\\n<html>\\n<head>\\n<link \
    rel=\\"stylesheet\\" href=\\"/common/style/opensans.css\\" type=\\"text/css\\">\\n<link \
    rel=\\"stylesheet\\"</html>\\n"], "regex_match": ["a;A;"], "regex_pattern": ["a"]}'

    def test_serialization(self, mock_redis_input):
        res = RedisWrapper.serialize_data(mock_redis_input)
        assert res['url'] == 'http://www.fit.vutbr.cz/'
        assert res['job'] == 'basicplatform'
        assert res['regex_match'] == 'a;A;'
        assert res['regex_pattern'] == 'a'
        assert res['headers'].startswith("{b'Date'")
        assert res['htmlcontent'].startswith('<!DOCTYPE')
