import asyncio
import pytest

from pharty3 import AIOLemmiwinks
from pharty3 import DBStorageData
from pharty3 import DBStorageJobs
from pharty3 import db_engine

loop = asyncio.get_event_loop()


class TestDB:
    @pytest.fixture(scope='module')
    def lemmiwinks(self):
        lemmiwinks = AIOLemmiwinks()
        yield lemmiwinks
        DBStorageData.__table__.drop(db_engine)
        DBStorageJobs.__table__.drop(db_engine)

    @pytest.mark.filterwarnings('ignore:ClientSession')
    def test_connection(self, lemmiwinks):
        assert lemmiwinks.db_session is not None

    @pytest.fixture(scope='function')
    def mock_db_data(self):
        mock = dict()
        mock['regex_pattern'] = '[A-Z][0-9]mock.+'
        mock['regex_match']   = 'A0mock1;B1mockq'
        mock['archive_name']  = '/tmp/archived_mock/zptuv5xf7t3am5'
        mock['job']           = 'job_mock'
        mock['url']           = 'http://www.mock.net'
        mock['spider']        = 'spider_mock'
        mock['server']        = 'mock.net'
        mock['date']          = '2019-01-01'
        return mock

    @pytest.mark.filterwarnings('ignore:ClientSession')
    def test_storage_jobs(self, mock_db_data, lemmiwinks):
        loop.run_until_complete(lemmiwinks.store_to_db(mock_db_data))
        job = lemmiwinks.db_session.query(DBStorageJobs)
        assert job is not None
