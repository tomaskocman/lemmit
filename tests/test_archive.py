import asyncio
import os
import pytest

from pharty3 import AIOLemmiwinks
from pharty3 import DBStorageData
from pharty3 import DBStorageJobs
from pharty3 import db_engine

loop = asyncio.get_event_loop()


class TestArchivation:
    archive_name = 'tests/zptuv5xf7t3am5'

    @pytest.fixture(scope='module')
    def lemmiwinks(self):
        lemmiwinks = AIOLemmiwinks()
        yield lemmiwinks
        DBStorageData.__table__.drop(db_engine)
        DBStorageJobs.__table__.drop(db_engine)

    @pytest.fixture(scope='function')
    def mock_html_data(self):
        mock = dict()
        mock['regex_pattern'] = '[A-Z][0-9]mock.+'
        mock['regex_match'] = 'A0mock1;B1mockq'
        mock['archive_name'] = TestArchivation.archive_name
        mock['job'] = 'job_mock'
        mock['url'] = 'http://www.mock.net'
        mock['spider'] = 'spider_mock'
        mock['server'] = 'mock.net'
        mock['date'] = '2019-01-01'
        mock['headers'] = None
        mock['htmlcontent'] = '''
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html>
<head>
<link rel="stylesheet" href="/common/style/opensans.css" type="text/css">
<link rel="stylesheet" href="/common/style/style.css" type="text/css">
<!--[if lte IE 6]>
   <link rel="stylesheet" type="text/css" href="/common/style/ie.css">
<![endif]-->
<link rel="stylesheet" href="/common/style/print.css" type="text/css" media="print">
<link type="application/rss+xml" rel="alternate" title="FIT News" href="http://www.fit.vutbr.cz/news/news-rss.php">
<meta name="googlebot" content="noarchive">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Campus Božetěchova</title>
<script type="text/javascript">
<!--
if (top.frames.length!=0) top.location=self.document.location;
function noenter()
{
  return !(window.event && window.event.keyCode == 13);
}
// -->
</script>
</head>
<body><!--noindex-->
<div id="page" class="inner lowhead crumb en">
<div class="container">
<div id="header">
<div class="left"></div>
<div class="main"><a href="/"><img src="/images/fitnewz.png" height="39" width="96" alt="Logo FIT"><span>Faculty of Information Technology</span></a>
</div>
<div class="right"></div>
</div>
<div id="toolbox">
<p>
<a class="lang" title="Česká verze" href="/FIT/location/index.php.cs"><span>Česká verze</span></a>
<a class="hidden" href="#contentbox">Skip main menu</a>
<a class="hidden" href="#content">Skip to body</a>
</p><form action="/search.php" method="get">
<p><label for="search">Search:</label>
<input style="color: #7f7f7f" id="search" size=20 type="text" name="q" value="Search" onClick="if(this.value=='Search'){this.value='';}">
<input type="hidden" name="form" value="extended">
<input type="hidden" name="sp" value="1">
<input type="hidden" name="lang" value="en">
<input class="submit" type="image" src="/common/style/images/search.png" alt="Search">
</p>
</form>
</div>
<!--/noindex-->

<!--noindex-->
<div id="menuline">
<div class="left"></div>
<div class="main">
<h1 class="hidden">Nacházíte se v sekcích</h1>
<ul>
<li><a href="/">Faculty</a></li>
<li><a href="/FIT/">About the Faculty
</a></li>
<li><a href="/FIT/location/">Campus
</a></li>
</ul>
</div>
<div class="right"></div>
</div>
<!--/noindex-->
<!--noindex-->
<div id="menu">
<h1 class="hidden">Main menu</h1>
<div class="col">
<ul><li><a href="/"><span>Home page</span></a>
<li class="selected"><a href="/FIT/"><span><em>About the Faculty</em></span></a>
<ul><li><a href="/FIT/contact/"><span>Contact</span></a>
<li><a href="/FIT/history/"><span>History</span></a>
<li><a href="/FIT/org/"><span>Organization structure</span></a>
<li><a href="/FIT/vz/"><span>Annual Reports</span></a>
<li class="selected"><a href="/FIT/location/"><span>Campus</span></a>
<ul><li><a href="/FIT/location/travel.php"><span>Directions</span></a>
</ul><li><a href="/FIT/map/fit1.php"><span>Map of the Campus</span></a>
<li><a href="/FIT/museum/"><span>Museum</span></a>
<li><a href="/FIT/dz/"><span>Strategic plan</span></a>
<li><a href="/FIT/awards/"><span>Awards</span></a>
<li><a href="/FIT/management/"><span>Management</span></a>
<li><a href="/FIT/AS/"><span>Academic Senate</span></a>
<li><a href="/FIT/VR/"><span>Scientific Board</span></a>
<li><a href="/FIT/dk/"><span>Disciplinary Board</span></a>
<li><a href="/FIT/PR/"><span>Industrial Board</span></a>
</ul><div class="bottom"></div><li><a href="/admissions/"><span><em>Admissions</em></span></a>
<li><a href="/study/"><span><em>Study</em></span></a>
<li><a href="/research/"><span><em>Research</em></span></a>
<li><a href="/cooperation/"><span><em>Cooperation</em></span></a>
<li><a href="/info/"><span><em>Official Documents</em></span></a>
<li><a href="/units/"><span><em>Departments</em></span></a>
<li><a href="/CVT/"><span><em>Computer Centre</em></span></a>
<li><a href="/lib/"><span><em>Library</em></span></a>
<li><a href="/events/"><span><em>Events</em></span></a>
<li><a href="/news/"><span><em>News</em></span></a>
<li><a href="/links/"><span><em>Links</em></span></a>
<li><a href="/search.php"><span><em>Search</em></span></a>
</ul>
</div>
</div>
<!--/noindex-->
<div id="main">
<div class="content">
<table id="contenttable"><tr><td>
<h1>Campus Božetěchova</h1>
<img src="IMG_5526p.jpg" border=1 alt=""><br clear=all><p>Faculty is located in the former 
<A HREF="history.html">Carthusian monastery</A>, founded in 1375. 

<UL>
<li><a href="http://www.mapy.cz/s/2baY"><img src="mapy.gif" alt="Seznam Mapy: You can find us here" width=88 height=31 border=1></a> 
<li><a href="travel.php">Directions to the Campus</a>
<li><a href="/FIT/map/">Map of the Campus</a>
<li><a href="/news/it4i/">New research building construction in 2012-2013</a>
<li><a href="/news/vrb1/">New buildings and reconstruction in 2004-2008</a>
<li><a href="bozka03/ulice.html">Božetěchova street after partial facade repair in the 2002</a>
<li><a href="bozka03/jih.html">South gothic cells after reconstruction in the 2002</a>
<li><a href="bozka03/sever.html">North gothic cells after facade repair in the 2001/2</a>
<li><a href="bozka03/pk.html">New lecture halls founded in the 2001/2</a>
<li><a href="pk/">Construction of the new lecture halls (2001/2)</a>
</UL>

<h2>Useful links</h2>
<ul>
<li><a href="http://www.brno.cz/">City of Brno</a>
<li><a href="http://www.brno-airport.cz/en/">Brno airport</a>
<li><a href="http://www.dpmb.cz/">Brno public transport company</a> (maps, timetables)

</UL></td></tr></table>
</div>
</div>

<!--noindex-->
<div id="undercol"><p>Your IPv4 address: 109.81.178.61<br><a href="http://www6.fit.vutbr.cz/">Switch to IPv6 connection</a><p><a href="http://www.dnssec-validator.cz/">DNSSEC <img src="/common/img/dnssec.png" style="vertical-align:middle" alt="[dnssec]"></a>
</div>
<!--/noindex-->
<div id="footer">
<div class="content"><!--noindex-->
<address>&copy; Faculty of Information Technology, BUT, Božetěchova&nbsp;1/2,<br> 612 66 Brno, Czech Republic<br>Tel.: +420 54114 1144, Fax: +420 54114 1270<br>
E-mail: <a href="mailto:info@fit.vutbr.cz">info@fit.vutbr.cz</a>,
Web: <a href="http://www.fit.vutbr.cz/">http://www.fit.vutbr.cz/</a></address><!--/noindex-->
</div>
</div>
</div>
</div>
</body>
</html>
'''
        yield mock
        os.remove(TestArchivation.archive_name + '.maff')

    @pytest.mark.filterwarnings('ignore:ClientSession')
    def test_archive_page(self, mock_html_data, lemmiwinks):
        loop.run_until_complete(lemmiwinks.archive_task(mock_html_data))
        assert os.path.isfile(TestArchivation.archive_name + '.maff')
