import asyncio
import ast
import json
import random
import string
from os import path
from tempfile import NamedTemporaryFile

from sqlalchemy import create_engine, Column, String, ForeignKey
from sqlalchemy.orm import sessionmaker
from sqlalchemy.exc import OperationalError
from sqlalchemy.exc import ProgrammingError
from sqlalchemy.ext.declarative import declarative_base
from aioredis.commands import Redis, create_redis

import settings
from logging import getLogger
from lemmiwinks import httplib, archive, pathgen, parslib
from lemmiwinks.archive import migration
from lemmiwinks.lemmiwinks import Lemmiwinks, Logger

Base = declarative_base()
db_engine = create_engine(settings.DB_URL, echo=settings.DB_DEBUG)

db_connection = db_engine.connect()
try:
    Logger.get_logger('Global scope').info(f'Database {settings.DB_URL}/{settings.DB_NAME} is going to be created')
    db_connection.execute('commit')
    db_connection.execute(f'create database {settings.DB_NAME}')
except ProgrammingError as e:
    Logger.get_logger('Global scope').warning(f'Database {settings.DB_URL}/{settings.DB_NAME} already exists')
finally:
    db_connection.close()

Session = sessionmaker(bind=db_engine)


class RedisWrapper:
    """Wrapper for asynchronous redis service.
    Allows creating and closing of redis connection and seding commands.
    """
    def __init__(self, address: str):
        """Initializes object with empty redis connection.
        Sets redis address to be used for creation of connection.

        Args:
            address: Address of redis service.
        """
        self.redis = None
        self.address = address

        self.logger = Logger.get_logger(self.__class__.__name__)
        self.logger.debug(f'{self.__class__.__name__} class initialized')

    async def create_connection(self):
        """Creates redis connection."""
        self.redis = await create_redis(self.address)
        self.logger.debug(f'Redis established connetion with {self.address}')

    async def close_connection(self):
        """Closes redis connection."""
        self.redis.close()
        await self.redis.wait_closed()
        self.redis = None

        self.logger.debug(f'Redis closes connection with {self.address}')

    @property
    def connection(self) -> 'Redis':
        """Gets redis object for allowing making commands.

        Returns:
            Redis object.
        """
        return self.redis

    @staticmethod
    def serialize_data(data: bytes) -> dict:
        """Redis datatype list returns python type[list], so we grab only first element, decode it and convert to dict.

        Args:
            data: Raw data from redis to be serialized.

        Returns:
            First element of serialized data.

        Raises:
            JSONDecodeError: Data cannot be converted into the json format.
        """
        data_dict = {}

        try:
            data = json.loads(data.decode())
        except json.JSONDecodeError as e:
            Logger.get_logger(__class__.__name__).error('Cannot serialize data - probably invalid json format!')
            raise e

        for key, value in data.items():
            data_dict[key] = value[0]

        return data_dict


class ArchiveSettings(migration.MigrationSettings):
    """Settings to be used in archivation process."""
    def __init__(self, headers):
        self.headers = headers

    @property
    def http_client(self):
        return httplib.provider.ClientFactory(httplib.client.AIOClient, headers=self.headers).singleton_client

    @property
    def css_parser(self):
        return parslib.provider.CSSParserProvider.tinycss_parser

    @property
    def html_parser(self):
        return parslib.provider.HTMLParserProvider.bs_parser

    @property
    def resolver(self):
        return httplib.resolver.URLResolver

    @property
    def path_gen(self):
        return pathgen.FilePathProvider.filepath_generator

    @property
    def http_js_pool(self):
        return httplib.ClientPool


class DBStorageJobs(Base):
    """Database table jobs.
    Stores job name and regex_pattern.
    """
    __tablename__ = 'jobs'

    job           = Column(String(255), nullable=False, primary_key=True)
    regex_pattern = Column(String(255), nullable=False)


class DBStorageData(Base):
    """Database table data.
    Stores properties about particular job.
    """
    __tablename__ = 'data'

    path        = Column(String(255), nullable=False, primary_key=True)
    job         = Column(String(255), ForeignKey(DBStorageJobs.job, ondelete='RESTRICT'), nullable=False)
    url         = Column(String(1000), nullable=False)
    spider      = Column(String(255), nullable=False)
    server      = Column(String(255), nullable=False)
    date        = Column(String(255), nullable=False)
    regex_match = Column(String(255), nullable=False)


class AIOLemmiwinks(Lemmiwinks):
    """Contains whole logic of archiving of sites.
    Grabs data from redis and archive it.

    Raises:
        RuntimeError: If redis database does not work.
    """
    def __init__(self):
        """Initializes redis instance, logger and database ORM."""
        super().__init__()

        self.redis = RedisWrapper(settings.REDIS_ADDRESS)
        self.redis_queue = settings.REDIS_QUEUE

        self.client = httplib.ClientFactoryProvider.aio_factory.singleton_client(pool_limit=500, timeout=10)

        # Create and commit db schema.
        try:
            Base.metadata.create_all(db_engine)
        except OperationalError:
            self.logger.error("PostgreSQL database is not running. Start PostgreSQL service or PostgreSQL docker image")
            raise RuntimeError("PostgreSQL database is not running")

        self.db_session = Session()
        self.db_session.commit()

        self.delay_time = 0
        self.stop_condition = False

        self.logger = getLogger(self.__class__.__name__)
        self.logger.debug(f'{self.__class__.__name__} class initialized')

    async def task_executor(self):
        """Loops over items in redis queue.
        Serializes items and archives htmlcontent.
        """
        await self.redis.create_connection()

        while not self.stop_condition:
            result = await self.redis.connection.lpop(self.redis_queue)
            # If there are no data in redis, delay is multiplied by 2 up to 64s.
            if not result:
                if self.delay_time <= 32:
                    if self.delay_time == 0:
                        self.delay_time = 1
                    self.delay_time *= 2

                self.logger.warning(f'There are no data in redis, delay time is {self.delay_time}s')
                await asyncio.sleep(self.delay_time)
                continue

            # Data found in redis. Reset delay time.
            self.delay_time = 0

            try:
                result = RedisWrapper.serialize_data(result)
            except json.JSONDecodeError:
                self.logger.error('Cannot serialize json data... skipping this archivation')
                continue

            self.logger.info(f'Got data from redis, URL: {result["url"]}')

            # Generate random archive name. 8 aplhanumeric characters.
            result['archive_name'] = 'archived/' + ''.join(random.choices(string.ascii_letters + string.digits, k=16))

            headers_bytes = ast.literal_eval(result['headers'])
            headers = {k.decode('utf-8'): v[0].decode('utf-8') for k, v in headers_bytes.items()}
            result['headers'] = headers

            task = self.archive_task(result)
            await asyncio.gather(task)

        await self.redis.close_connection()

    async def archive_task(self, content: dict):
        """Archives page into maff format.

        Args:
            content: Dict containing all informations about archived page.
        """
        self.logger.info('[START] Archive new data')

        content_descriptor = self.create_content_descriptor(content['htmlcontent'])
        url_and_status = [(content['url'], 200)]
        container_response = httplib.container.Response(content_descriptor, url_and_status)
        await self.archive_response(container_response, content['archive_name'], content['headers'])

        self.logger.info('[OK] Archive new data')

        content['archive_name'] = path.abspath(content['archive_name']) + '.maff'
        await self.store_to_db(content)

        self.logger.info(f'Stored path of archived data ({content["archive_name"]}) to the database')

    async def archive_response(self, container_response: httplib.container.Response, archive_name: str, http_headers: dict):
        """Creates envelope from response and archives it.

        Args:
            container_response: Contains html file.
            archive_name: Name of resulting archive.
            http_headers: Http headers to be added to subsequent requests.
        """
        archive_setting = ArchiveSettings(http_headers)
        letter = archive.SaveResponseLetter(container_response, archive_setting, archive.Mode.NO_JS_EXECUTION)

        envelope = archive.Envelop()
        envelope.append(letter)

        self.logger.debug('Created archivation envelope')

        await archive.Archive.archive_as_maff(envelope, archive_name)

    async def store_to_db(self, content: dict):
        """Stores data into the database using ORM. Test database is Postgresql.
        Insertion into DBStorageJobs table is performed only once in the beginning.

        Args:
            content: Dict of data to be stored.
        """
        if not self.db_session.query(DBStorageJobs).filter_by(job=content['job']).first():
            self.db_session.add(DBStorageJobs(job=content['job'], regex_pattern=content['regex_pattern']))
            self.db_session.commit()

        self.db_session.add(
            DBStorageData(
                path=content['archive_name'], job=content['job'],
                url=content['url'], spider=content['spider'],
                server=content['server'], date=content['date'],
                regex_match=content['regex_match']
            )
        )

        self.db_session.commit()

    def create_content_descriptor(self, html_content: str):
        """Creates content descritor using temporary file.

        Args:
            html_content: String containing html page.

        Returns:
            Content descriptor using temporary file.
        """
        content_descriptor = NamedTemporaryFile()
        content_descriptor.write(html_content.encode())
        self.logger.debug('Content desriptor was created')

        return content_descriptor


if __name__ == '__main__':
    """Create and run asyncio event loop."""
    aio_archive = AIOLemmiwinks()

    aio_archive.run()
    aio_archive.stop()
